require 'csv'
namespace :messages do
  desc "extract message info"
  task :export => [:environment] do
    csv_string = CSV.generate do |csv|
      csv << Message.attribute_names
      Message.where('name IS NOT NULL').each do |user|
        csv << user.attributes.values
      end
    end
    p "*******"
    p csv_string
    p "*******"
    csv_string = CSV.generate do |csv|
      csv << Message.attribute_names
      Message.where('name IS NULL').each do |user|
        csv << user.attributes.values
      end
    end
    p "*******"
    p csv_string
    p "*******"
    
  end
end
