class CreateBackendUsers < ActiveRecord::Migration
  def change
    create_table :backend_users do |t|
      t.string :email
      t.string :encrypted_password
      t.string :salt
      t.string :confirmation_token
      t.string :remember_token
      t.string :roles
      t.string :display_name

      t.timestamps null: false
    end
  end
end
