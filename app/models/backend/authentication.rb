module Backend::Authentication

  def self.included(controller) # :nodoc:
   controller.send(:include, InstanceMethods)
   controller.extend(ClassMethods)
  end

  module ClassMethods
   def self.extended(controller)
     controller.helper_method :current_backend_user, :backend_user_signed_in?, :backend_user_signed_out?
     controller.hide_action   :current_backend_user, :current_backend_user=,
                              :backend_user_signed_in?,   :backend_user_signed_out?,
                              :backend_user_sign_in,      :backend_user_sign_out,
                              :backend_authenticate, :backend_deny_access
   end
  end

  module InstanceMethods
    # User in the current cookie
    #
    # @return [User, nil]
    def current_backend_user
      @_current_backend_user ||= backend_user_from_cookie
    end

    # Set the current user
    #
    # @param [User]
    def current_backend_user=(user)
      @_current_backend_user = user
    end

    # Is the current user signed in?
    #
    # @return [true, false]
    def backend_user_signed_in?
      ! current_backend_user.nil?
    end

    # Is the current user signed out?
    #
    # @return [true, false]
    def backend_user_signed_out?
      current_backend_user.nil?
    end

    # Deny the user access if they are signed out.
    #
    # @example
    #   before_filter :authenticate
    def backend_authenticate
      deny_access unless backend_user_signed_in?
    end

    # Sign user in to cookie.
    #
    # @param [User]
    #
    # @example
    #   sign_in(@user)
    def backend_user_sign_in(user)
      if user
        cookies[:remember_token] = {
          :value   => user.remember_token,
          :expires => Clearance.configuration.cookie_expiration.call(cookies)
        }
        self.current_backend_user = user
      end
    end

    # Sign user out of cookie.
    #
    # @example
    #   sign_out
    def backend_user_sign_out
      current_backend_user.reset_remember_token! if current_backend_user
      cookies.delete(:remember_token)
      self.current_backend_user = nil
    end

    protected

    def backend_user_from_cookie
        token = ""
        token = cookies[:remember_token] if cookies[:remember_token]
        if token.length > 0 || token.nil?
          ::BackendUser.find_by_remember_token(token)
        else
          token = request.headers['Authorization']
          ::BackendUser.find_by_remember_token(token)
        end
    end
  end
 end