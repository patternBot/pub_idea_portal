class BackendUser < ActiveRecord::Base
  include Clearance::User
  validates_presence_of :roles, :display_name
  validates_inclusion_of :roles ,:in => [ "admin"]
  
  def admin?
    !!(self.roles =~ /admin/)
  end
end
