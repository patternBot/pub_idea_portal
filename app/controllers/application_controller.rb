class ApplicationController < ActionController::Base
  include Clearance::Controller
  protect_from_forgery
  skip_before_filter :verify_authenticity_token
  
  def not_found
    raise ActionController::RoutingError.new('Not Found')
  end

  def logged_in_access
    unless current_user
      flash[:error] = "You are not authorized to perform this action"
      render :nothing => true, :status => :unauthorized
    end
  end
end

