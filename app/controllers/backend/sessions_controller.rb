class Backend::SessionsController < Backend::BackendController
  prepend_before_filter :ensure_not_loggedin, :only => [:new, :create]
  
  def new
  end

  def create
    if params[:email].nil? || params[:password].nil?
      render :json => {}, :status => :unprocessable_entity
    else
      @user = BackendUser.authenticate(params[:email], params[:password])
      if @user.nil?
        render :json => {}, :status => :unauthorized
      else
        backend_user_sign_in(@user)
        @messages = Message.all
        render 'backend/messages/index'
      end
    end
  end
  
  def destroy
    backend_user_sign_out
    flash[:success] = translate(:signed_out, :default =>  "Signed out.")
    redirect_to(backend_home_path)
  end

  def ensure_not_loggedin
    if backend_user_signed_in?
      flash[:notice] = "Please logout to login as different user"
      #redirect_back_or(backend_home_path)
      respond_to do |format|
        format.html { redirect_back_or(backend_messages_path) }
        format.json  { render :json => {:error => "You are not authorized to perform this action"} , :status => :unauthorized }
      end
    end
  end 
end