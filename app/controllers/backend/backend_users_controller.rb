class Backend::BackendUsersController < Backend::BackendController

  def show
    begin
      @backend_user=BackendUser.find(params[:id])
      render :json=>@backend_user , :status => :ok
    rescue Exception => e
      render :nothing => true , :status => :unprocessable_entity
    end
  end

end