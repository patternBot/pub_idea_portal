class Backend::BackendController < ApplicationController
  include Backend::Authentication

  def admin_access
    unless current_backend_user && (current_backend_user.admin? )
      respond_to do |format|
        format.html { redirect_to root_path }
        format.json  { render :json => {:error => "You are not authorized to perform this action"} , :status => :unauthorized }
      end
    end
  end

private
  def redirect_to_back_or_default(default = root_url)
    if request.env["HTTP_REFERER"].present? and request.env["HTTP_REFERER"] != request.env["REQUEST_URI"]
      redirect_to :back
    else
      redirect_to default
    end
  end

end