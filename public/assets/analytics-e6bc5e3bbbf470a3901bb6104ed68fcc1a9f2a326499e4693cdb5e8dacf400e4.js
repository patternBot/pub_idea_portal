var ready;
ready = function() {

    if (window._gaq != null) {
     return _gaq.push(['_trackPageview']);
    } else if (window.pageTracker != null) {
     return pageTracker._trackPageview();
    }
};

$(document).ready(ready);
$(document).on('page:load', ready);
